class Persona:
    saldo=None
    comision=None
    nombre=None

    def __init__(self,nombre :str,saldo:int,comision:int):
        self.comision=comision
        self.saldo = saldo
        self.nombre = nombre
    
    def __str__(self):
        return f"La persona {self.nombre} tiene {self.saldo} pesos y la comision es de: {self.comision}"


def aumento_comision(persona:Persona)->Persona:
    persona.comision*=2
    return persona

personas =[
    Persona(nombre="roberto",saldo=200,comision=12),
    Persona(nombre="gabriela",saldo=1200,comision=11.45),
    Persona(nombre="arturo",saldo=23200,comision=30)
]

for persona in personas:
    print(persona)

personas = map(aumento_comision,personas)

for persona in personas:
    print(persona)