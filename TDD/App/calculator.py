class Calculator:
	def suma(self,num1,num2):
		return num1+num2

	def resta(self,num1,num2):
		return num1-num2

	def multiplicacion(self,num1,num2):
		return num1*num2
	
	def division(self,num1,num2):
		return num1/num2

	def jsonData(self):
		data = {
			'key_1': "hola",
			'key_2': {
				'key_1_1':"mundo",
				'key_1_2':3,
				'key_1_3':5.8,
				'key_1_4':[1,2,5.5,'tutorial'],
				'key_1_5':(1,2,5.5,'perro')
			}
		}
		return data