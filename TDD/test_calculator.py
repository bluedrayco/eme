import os
import pytest
from App.calculator import Calculator
import pdb


def test_calculator_methods_exist():
	calculator = Calculator()
	assert calculator.suma != None
	assert calculator.resta != None
	assert calculator.multiplicacion != None
	assert calculator.division != None
	# pdb.set_trace()

def test_suma():
	calculator = Calculator()
	numero1 = 10
	numero2 = 12
	assert calculator.suma(numero1,numero2) == 22
	numero1 = -3
	numero2 = -8
	assert calculator.suma(numero1,numero2) == -11

def test_division():
	calculator = Calculator()
	numero1=5
	numero2=10
	# pdb.set_trace()
	assert calculator.division(numero1,numero2) == 0.5
	assert calculator.division(numero1,numero1) == 1.0
	assert calculator.division(numero2,numero1) == 2.0
	with pytest.raises(ZeroDivisionError):
		calculator.division(numero1,0)

def test_multiplicacion():
	calculator = Calculator()
	numero1=3
	numero2=0.5
	# pdb.set_trace()
	assert calculator.multiplicacion(numero1,numero2) == 1.5
	numero1=0
	numero2=0
	assert calculator.multiplicacion(numero1,numero2) == 0
	numero1=0
	numero2=5
	assert calculator.multiplicacion(numero1,numero2) == 0

def test_resta():
	calculator = Calculator()
	numero1 = -3
	numero2 = 1
	assert calculator.resta(numero1,numero2) == -4
	assert calculator.resta(numero2,numero1) == 4
	numero1 = 0
	numero2 = 0
	assert calculator.resta(numero1,numero2) == 0

def test_json_data():
	calculator = Calculator()
	data = calculator.jsonData()
	arreglo = []
	pdb.set_trace()

