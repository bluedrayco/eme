class Clase1:
    def __init__(self):
        self.atributo1=0

class Clase2:
    def __init__(self):
        self.atributo2=10

class Clase3(Clase1,Clase2):
    
    def __init__(self):
        Clase1.__init__(self)
        Clase2.__init__(self)

objeto = Clase3()
print(dir(objeto))
