def generaPares(numero):
    i=1
    while i<numero:
        yield i*2
        i+=1

numeros =generaPares(22)
numeros3 = list(generaPares(22))
# for i in numeros:
#     print (i,end=",")

#una vez que el cursor termina en los generators estos terminan su pointer
numeros2= [i for i in numeros]

# print(numeros2)



# print(numeros3)

def sumar_de_x_en_x(numero,final):
    i=0
    while i<=final:
        yield i
        i +=numero

#range es tambien un generador
lista_numeros = [i for i in sumar_de_x_en_x(5,100)]

# print(dir(lista_numeros))

def contador(max):
    print("=Dentro de contador - empezando")
    n=0
    while n < max:
        print(f"=Dentro de contador - viene yield con n={n}")
        yield n
        print("=Dentro de contador - retomando después de yield")
        n=n+1
    print("=Dentro de contador - terminando")

print("Instanciando contador") 
mycont = contador(3)
print("Contador instanciado") 

for i in mycont:
    print(f"valor leido del iterador={i}") 
print("Listo") 