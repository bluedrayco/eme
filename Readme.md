# instalar virtualenv
pip install virtualenv

# crear un entorno virtual en python3
virtualenv venv --python=python3

# acceder al virtualenv
source venv/bin/activate

# salir del virtualenv
deactivate

# ejecutar archivo de requirements para instalar dependencias
pip install -r requirements.txt

# instalar una sola dependencia
pip install pytest==5.3

# array
## create array
arreglo = []
## add element to array to final order
arreglo.append('hola')
## extract last item from array
arreglo.pop()
## extract item specifying the index
arreglo.pop(index):value
## find and extract item specifying (only the first ocurrence)
arreglo.remove(element)
## find element and get index of the element
arreglo.index(element):int
## slicing array
# creating a list  
a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]  
# using Slice operation 
Sliced_array = a[3:8] 
# pre-defined point to end 
Sliced_array = a[5:] 
# beginning till end 
Sliced_array = a[:] 


# dictionaries
## get keys
keys = dictionary.keys() 
## iterate them
statesAndCapitals = { 
                     'Gujarat' : 'Gandhinagar', 
                     'Maharashtra' : 'Mumbai', 
                     'Rajasthan' : 'Jaipur', 
                     'Bihar' : 'Patna'
                    } 

for state in statesAndCapitals: 
    statesAndCapitals[state] 

# strings
## split string
text.split(delimiter)
## join 
"text_to_join_each_element".join(array)

# exceptions
try:
  print(x)
except:
  print("An exception occurred")

try:
  raise NameError()
except NameError as e:
  print("Variable x is not defined")
except:
  print("Something else went wrong")


# format in print
print ("pyton number: %d" % (22))