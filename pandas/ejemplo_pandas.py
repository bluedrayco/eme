import pandas as pd
import numpy as np

data ={
    'Nombre':['roberto','ramiro','ezequiel'],
    'Calificaciones':[100,80,90],
    'Materias':['Espanol','Matematicas','Biologia'],
    'Deportes':['Futbol','Basquetbol','Beisbol']
}

df = pd.DataFrame(data)

print(df)
print('\n'*2)


data_2 ={
    'Nombre':['roberto','ramiro','N/A'],
    'Calificaciones':[np.nan,'80','90'],
    'Materias':['Espanol','Matematicas','Biologia'],
    'Deportes':['Futbol','N/A','Basquetbol']
}

df2=pd.DataFrame(data_2)

print(df2)
print(df2.info())
print(df2.describe())

nuevo = pd.DataFrame(df2)
nuevo = nuevo.replace(np.nan,'0')

print(nuevo)

nuevo2 = pd.DataFrame(df2)
nuevo2.dropna(how='any',inplace=True)

print(nuevo2)

#eliminar registro por columna
df2 = df2[df2['Nombre']!='N/A']
columna= df2[df2['Deportes']!='N/A']
print('\n'*2)
print(columna)

print('\n'*2)
