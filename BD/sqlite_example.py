import sqlite3
from sqlite3 import (
    IntegrityError
)
import logging
logger = logging.getLogger()
breakpoint()
hdlr = logging.FileHandler('logger.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)

miConexion = sqlite3.connect('mi_base_datos.db')

cursor = miConexion.cursor()

cursor.execute("""
    CREATE TABLE IF NOT EXISTS PRODUCTOS (ID INTEGER PRIMARY KEY AUTOINCREMENT,CLAVE VARCHAR(5) UNIQUE,NOMBRE_ARTICULO VARCHAR(50),PRECIO INTEGER,SECCION VARCHAR(20))
""")

# cursor.execute("INSERT INTO PRODUCTOS VALUES(NULL,'PRODUCTO 1',50,'CATEGORIA 1')")

productos =[
    ("ABCDE","camisetas",500,"categoria 1"),
    ("12345","balon",50,"categoria 2"),
    ("22334","zapatos",200,"categoria x"),
    ("76543","playera",1200,"categoria y"),
    ("12340","bermuda",1100,"categoria y")
]

try:
    cursor.executemany("INSERT INTO PRODUCTOS (CLAVE,NOMBRE_ARTICULO,PRECIO,SECCION) VALUES (?,?,?,?)",productos)
    miConexion.commit()
except IntegrityError as e:
    logger.error(e)
    miConexion.rollback()

cursor.execute('select * from productos')
productos = cursor.fetchall()

print (productos)
miConexion.close()