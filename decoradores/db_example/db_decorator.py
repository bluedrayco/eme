import sqlite3
from sqlite3 import (
    IntegrityError
)


class DBConnection:
    __connection = None

    def __init__(self,database_name="mi_base_datos.db"):
        self.__connection = sqlite3.connect(database_name)
        InitializeBD(self.__connection)

    def __call__(self, func):
        def wrapped_function(*args, **kwargs):
                if not ("connection" in kwargs.keys()):
                    kwargs['connection']=self.__connection
                return func(*args, **kwargs)
        return wrapped_function


class InitializeBD:
    __connection = None
    __usuarios =[
        ("Roberto Leroy","Monroy","Ruiz",30),
        ("Guadalupe","Ruiz","Ayala",50),
        ("Jose Alejandro","Salvador","Ruiz",16)
    ]

    def __init__(self,connection):
        self.__connection = connection
        self.__crearTablaUsuarios()
        self.__poblarTablaUsuarios()


    def __crearTablaUsuarios(self):
        cursor = self.__connection.cursor()
        cursor.execute("""
            CREATE TABLE IF NOT EXISTS USUARIOS 
                (
                    ID INTEGER PRIMARY KEY AUTOINCREMENT,
                    NOMBRE VARCHAR(50),
                    APELLIDO_PATERNO VARCHAR(50),
                    APELLIDO_MATERNO VARCHAR(50),
                    EDAD INTEGER
                )
        """)

    def __poblarTablaUsuarios(self):
        cursor = self.__connection.cursor()
        cursor.executemany("INSERT INTO USUARIOS (NOMBRE,APELLIDO_PATERNO,APELLIDO_MATERNO,EDAD) VALUES (?,?,?,?)",self.__usuarios)
        self.__connection.commit()
