from db_decorator import DBConnection
from sqlite3 import (
    IntegrityError
)


@DBConnection(database_name='base_datos_x.db')
def getUsuarios(connection=None):
    cursor = connection.cursor()
    cursor.execute('select * from usuarios')
    usuarios = cursor.fetchall()
    return usuarios


@DBConnection(database_name='base_datos_x.db')
def createUsuario(data, connection=None):
    try:
        cursor = connection.cursor()
        cursor.execute(
            "INSERT INTO USUARIOS (NOMBRE,APELLIDO_PATERNO,APELLIDO_MATERNO,EDAD) VALUES (?,?,?,?)", data)
        connection.commit()
    except IntegrityError as e:
        print(e)
        connection.rollback()
    return cursor.lastrowid


usuarioId = createUsuario(
    data=("Lourdes gabriela", "Rodriguez", "Rodriguez", 30))
usuarios = getUsuarios()
print(usuarios)
