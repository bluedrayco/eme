#decoradores sencillos
def division_zero(function_decorated):
    def method (*args, **kwargs):
        if args[1]==0:
            return "no se puede dividir por cero1111"
        else:
            return function_decorated(*args, **kwargs)

    return method


@division_zero
def division(a,b):
    return a/b

resultado = division(10,0)

print (resultado)

#decoradores con paso de parámetros
class DivisionPorZero:
    __message = None
    def __init__(self, texto='no se puede dividir por cero'):
        self.__message = texto

    def __call__(self, func):
        def wrapped_function(*args, **kwargs):
            breakpoint()
            if args[1]==0:
                return self.__message
            else:
                return func(*args, **kwargs)

        return wrapped_function

@DivisionPorZero(texto="la división está erronea, regresa más tarde")
def division2(a,b):
    return a/b

resultado2=division2(10,0)

print(resultado2)