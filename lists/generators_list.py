lista = [1,2,3,4,5,6]

# lista.clear()
lista.append(20)
lista.insert(3,99)
lista.extend([10,22,33,44]) # alternativa a array merge lista3= lista1+lista2
print(lista)
lista.index(3) #busca el index de la primer ocurrencia
lista.remove(44)
lista.pop() #ultimo elemento
del lista[3]
lista.count(2)

if 5 in lista:
    print("si se encuentra en lista")


print(lista)

print(lista[-2])

print (lista[-4:-2])

#multiplicar por dos old
arreglo_old = []
for x in lista:
    arreglo_old.append(x*2)

#multiplicar por dos con lambda
funcion_lambda=lambda x:x*2
arreglo_lambda = list(funcion_lambda(x) for x in lista)

#multiplicar por dos con generator
arreglo_generator = [int(x)*2 for x in lista]

print(arreglo_old)
print(arreglo_lambda)
print(arreglo_generator)

associative={
    "hola":"mundo",
    "perro":"gato"
}

for key in associative:
    print("el key es {} y el valor es {}".format(key,associative[key]))
    print("el key es {0} y el valor es {1}".format(key,associative[key]).upper())