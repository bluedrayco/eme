import pdb 

class Node:
    def __init__(self,data=None):
        self.data=data
        self.next=None


class Linked_List:
    def __init__(self):
        self.head = Node()
    
    def append(self,data):
        new_node = Node(data)
        cur = self.head
        while cur.next != None:
            cur = cur.next
        cur.next = new_node

    def length(self):
        cur = self.head
        total = 0
        while cur.next !=None:
            total +=1
            cur = cur.next
        return total
    
    def display (self):
        elems = []
        cur_node = self.head
        while cur_node.next !=None:
            cur_node = cur_node.next
            elems.append(cur_node.data)
        print (elems)
    
    def get (self,index):
        if(index >=self.length()):
            raise Exception("The index out of range")
        cur_idx=0
        cur_node=self.head
        while True:
            cur_node=cur_node.next
            if(cur_idx ==index): return cur_node.data
            cur_idx+=1
    
    def erase (self,index):
        if(index >=self.length()):
            raise Exception("The index out of range")
        pdb.set_trace()
        cur_idx=0
        cur_node=self.head
        while True:
            last_node = cur_node
            cur_node = cur_node.next
            if cur_idx == index:
                last_node.next = cur_node.next
                return
            cur_idx +=1




list = Linked_List()
list.append(1)
list.append(2)
list.append(3)
list.display()
value = list.get(2)

print ('element at 2nd index: %d' %value)

list.erase(1)
list.display()